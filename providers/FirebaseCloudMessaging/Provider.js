const {
  ServiceProvider
} = require('@adonisjs/fold')

class FirebaseCloudMessagingProvider extends ServiceProvider {
  register() {
    this.app.singleton('FirebaseCloudMessaging', () => {
      const Config = this.app.use('Adonis/Src/Config')
      return new(require('.'))(Config)
    })
  }
}

module.exports = FirebaseCloudMessagingProvider
